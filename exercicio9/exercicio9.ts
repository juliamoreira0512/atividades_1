/*Faça um programa que calcule e mostre a área de um
triângulo. Sabe-se que: Área = (base * altura)/2.
*/

//Início
namespace exercicio9 {
    //Entrada de dados
    let base, altura: number;

    base = 15;
    altura = 8;

    let area: number;

    //Processar dados
    area = (base * altura / 2);

    //Saída
    console.log("A área do triângulo é: " + area );
}
