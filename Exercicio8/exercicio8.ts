/* Faça um programa que receba o valor de um depósito e o
valor da taxa de juros, calcule e mostre o valor do rendimento
e o valor total depois do rendimento.
*/
//Início
namespace exercicio8 {
    //Entrada de dados
    let deposito, taxa: number;

    deposito = 1400;
    taxa = 30;

    let rendimento: number;

    rendimento = (deposito * taxa / 100) + deposito;

    console.log("O rendimento é de" + rendimento);

    let valor_total: number;
    valor_total = (rendimento + deposito);

    //Saída
    console.log("O valor total depois do rendimento é de: " + valor_total);
}