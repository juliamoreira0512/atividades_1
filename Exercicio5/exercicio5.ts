//Faça um programa que receba o salário de um funcionário e o percentual de aumento, calcule e mostre o valor do aumento e o novo salário.

//Início
namespace exercicio5 {
    let salario, percentual: number;

    salario = 1400;
    percentual = 30;

    let aumento: number;
    
    //Processar dados
    aumento = (salario * percentual / 100);

    //Saída
    console.log("O percentual de aumento do salário é:" + aumento );

    let novo_salario: number;
    novo_salario = salario + aumento;

    console.log("O valor do novo salário é:" + novo_salario );
}