//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o seu salário a receber, sabendo-se que esse funcionário tem gratificação de R$50,00 e paga imposto de 10% sobre o salário-base.

//Início
namespace exercicio7 {
    //Entrada de dados
    let salario_base: number;

    salario_base = 1400;

    let salario_gratificacao: number;

    //Processar
    salario_gratificacao = (salario_base + 50);

    let salario_imposto: number;
    salario_imposto = (salario_base * 10 / 100);

    let salario_final: number;

    salario_final = (salario_gratificacao - salario_imposto);

    //Saída
    console.log("O salário a receber é:" + salario_final);
}