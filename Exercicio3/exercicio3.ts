// Faça um programa que receba três notas, calcule e mostre a média ponderada entre elas.

//Início
namespace exercicio3
{
    let nota1, nota2, nota3, peso1, peso2, peso3: number;

    nota1 = 9;
    nota2 = 7;
    nota3 = 6;
    peso1 = 2;
    peso2 = 2;
    peso3 = 6;

    let media: number;
    
    //Processar dados
    media = (nota1 * peso1 + nota2 * peso2 + nota3 * peso3) / (peso1 + peso2 + peso3);

    //Saída
    console.log("A média ponderada entre as 3 notas é:" + media );
}