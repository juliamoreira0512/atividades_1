//Faça um programa que receba o salário-base de um funcionário, calcule e mostre o salário a receber, sabendo-se que esse funcionário tem gratificação de 5% sobre o salário base e paga imposto de 7% sobre o salário-base.

//Início
namespace exercicio6 {
    //Entrada de dados
    let salario_base: number;
    salario_base = 1400;

    let salario_imposto: number;

    //Processar os dados
    salario_imposto = (salario_base * 7 / 100);

    let salario_gratificacao: number;
    salario_gratificacao = (salario_base * 5 / 100);

    let salario_final: number;
    salario_final = (salario_base + salario_gratificacao - salario_imposto);

    //Saída
    console.log("O salário a receber é:" + salario_final);
}