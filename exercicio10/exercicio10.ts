/*Faça um programa que calcule e mostre a área de um
círculo. Sabe-se que: Área = π.R2
*/

//Início
namespace exercicio10 {
    //Entrada de dados
    let pi, rQuad, r, area_circulo: number;

    pi = Math.PI;
    r = 4;
    rQuad = r ** 2;
    area_circulo = pi * rQuad;

    //Saída
    console.log("A área do círculo é de: " + area_circulo);
}