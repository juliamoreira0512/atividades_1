//Faça um programa que receba o salário de um funcionário, calcule e mostre o novo salário, sabendo-se que este sofreu um aumento de 25%.

//Início
namespace exercicio4
{
    //Entrada
    let salario: number;
    
    salario = 1400;

    let novo_salario: number;
    
    //Processar dados
    novo_salario = salario + ( salario * 25 / 100 );

    //Saída
    console.log("O novo salário com aumento de 25% é:" + novo_salario );
}